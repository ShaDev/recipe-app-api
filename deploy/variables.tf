variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "info@shahna.tech"
}

# variable "db_username" {
#     description = "Username for the RDS postgres instance"
# }

# variable "db_password" {
#     description = "Password for the RDS postrest instance"
# }

# variable "bastion_key_name" {
#     default = "recipe-app-api-devops-bastion"
# }

# variable "ecr_image_api" {
#     description = "ECR image for API"
#     default = ""
# }

# variable "ecr_image_proxy" {
#     description = "ECR image for proxy"
#     default = ""
# }

# variable "django_secret_key" {
#     description = ""
# }

# variable "dns_zone_name" {
#     description = "Domain name"
#     default = "shahna.tech"
# }

# variable "subdomain" {
#     description = "Subdomain per environment"
#     type = map(string)
#     default = {
#         production = "api"
#         staging = "api.staging"
#         dev = "api.dev"
#     }
# }